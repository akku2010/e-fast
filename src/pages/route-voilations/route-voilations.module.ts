import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouteVoilationsPage } from './route-voilations';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    RouteVoilationsPage,
  ],
  imports: [
    IonicPageModule.forChild(RouteVoilationsPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class RouteVoilationsPageModule {}
